import java.util.Scanner;

public class Buscamines {
	static Scanner reader = new Scanner(System.in);
	static int mines[][] = new int[0][0];
	static int camp[][] = new int[0][0];
	static int caselleslliures=0;
	static String [] ranking=new String[5];
	

	public static void main(String[] args) {
		joc();

	}
	
	static void joc() {
		int opcio = 0;
		boolean opcioescollida = false;
		String nom="";
		int contadorguanyadors=0;

		do {
			// Mostrem el men�
			System.out.println("\n\nMen� d'opcions: ");
			System.out.println("1. Mostrar ajuda");
			System.out.println("2. Opcions");
			System.out.println("3. Jugar Partida");
			System.out.println("4. Veure Rankings");
			System.out.println("0. Sortir");
			System.out.print("Introdueix una opci�: ");

			boolean numcorrecte = false;
			
			
			while (!numcorrecte) {
				if (reader.hasNextInt()) {
					opcio = reader.nextInt();
					numcorrecte = true;
				} else {
					System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
					reader.next();
				}
			}

		switch (opcio) {
		
		case 0: // Sortir
			System.out.println("Ad�u!");
			break;
		case 1:
			mostraAjuda();
			break;
		case 2:
			nom=opcions(nom);
			opcioescollida=true;
			break;
		case 3:
			if(opcioescollida) {
				jugar(nom, contadorguanyadors);
			}else {
				System.out.println("Has de pasar pr�viament per la opci� 2 per poder jugar!");
			}
			
			break;
		case 4:
			ranking();
			break;

		default:
			System.out.println("Opci� incorrecta, ha d'estar entre 0 i 4");
			break;
		}
		}while (opcio != 0);

	}
	
	static void mostraAjuda() {
		System.out.println("El joc consisteix a buidar totes les caselles d'una pantalla que no ocultin una mina.\r\n" + 
				"Algunes caselles tenen un nombre, el qual indica la quantitat de mines que hi ha a les caselles circumdants.\r\n" + 
				"Si es descobreix una casella amb una mina es perd la partida."
				+ "\r\nPer jugar haur�s de passar per la opci� 2 i dir el teu nom, el tamany del tauler i el nombre de mines, posteriorment"
				+ " \r\nhaur�s de fer servir la opci� 3 per jugar, i la 4 per veure el nom dels jugadors que hagin superat"
				+ "\r\nel Buscamines."
				+ "\r\nLa opci� 0 acabar� el programa. Sort! ");
	
	}
	
	static String opcions(String nom) {
		int files=0;
		int columnes=0;
		int nummines=0;
		System.out.print("Escriu el teu nom: ");
		nom=reader.next();
		System.out.print("Escriu el nombre de files que vols que tingui el tauler, (m�nim 4, m�xim 25): ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				files = reader.nextInt();
				if(files>=26 || files<=3) {
					System.out.print("Les files han de ser de m�nim 4 i m�xim 25: ");
				}else {
					numcorrecte = true;
				}
				
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		System.out.print("Escriu el nombre de columnes que vols que tingui el tauler, (m�nim 4, m�xim 25): ");
		numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				columnes = reader.nextInt();
				if(columnes>=26 || columnes<=3) {
					System.out.print("Les columnes han de ser de m�nim 4 i m�xim 25: ");
				}else {
					numcorrecte = true;
				}
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		System.out.print("Escriu el nombre de mines que hi hagi al tauler: ");
		numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				nummines = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		caselleslliures=files*columnes-nummines;
		inicialitzarMines(files,columnes,nummines);
		inicialitzarCamp(files,columnes);
		return nom;
	}
	static void inicialitzarMines(int files, int columnes, int nummines) {
		mines = new int[files][columnes];
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				mines[f][c] = 0;
			}
		}

		int max = 0;
		int min = 0;
		int range =0;


		while (nummines > 0) {
			max = files - 1;
			min = 0;
			range = max - min + 1;
			int fila = (int) (Math.random() * range) + min;
			max = columnes - 1;
			min = 0;
			range = max - min + 1;
			int columna = (int) (Math.random() * range) + min;
			if(mines[fila][columna] == 1) {
				nummines++;
			}
			mines[fila][columna] = 1;
			nummines--;
		}
		

		
	}
	static void inicialitzarCamp(int files, int columnes) {
		camp = new int[files][columnes];
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				camp[f][c] = 9;
			}
		}
	
	}
	
	static void visualitzarCamp() {
		int i=0;
		System.out.print("C   ");
		while(i<camp[0].length) {
			if(i>=10) {
				System.out.print(i+" ");
			}else {
				System.out.print(i+"  ");
			}
			
			i++;
		}
		System.out.print("\nF\n");
		for (int f = 0; f < camp.length; f++) {
			if(f>=10) {
				System.out.print(f+"  ");
			}else {
				System.out.print(f+"   ");
			}
			for (int c = 0; c < camp[f].length; c++) {
				System.out.print(camp[f][c]+"  ");
			}
			System.out.println();
		}
	
	}
	static void jugar(String nom, int contadorguanyadors) {
		int f=0;
		int c=0;
		boolean partidaEnCurs=true;
		while(partidaEnCurs==true) {
			visualitzarCamp();
			f=demanaF(nom);
			c=demanaC(nom);
			descobrir(f,c);
			if(camp[f][c]==10) {
				System.out.println("HA ESTALLAT LA MINA!");
				visualitzarCamp();
				partidaEnCurs=false;
			}else if(caselleslliures==1) {
				System.out.println("HAS GUANYAT!"+nom+" ENHORABONA");
				partidaEnCurs=false;
				omplirRanking(nom, contadorguanyadors);
			}
			caselleslliures--;
		}
	}
	static int demanaF(String nom) {
		int f=0;
		System.out.print(nom+", escriu la fila a destapar: ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				f = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		return f;
	}
	
	static int demanaC(String nom) {
		int c=0;
		System.out.print(nom+", escriu la columna a destapar: ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				c = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.print("Ha de ser un n�mero enter, introdueix un altre: ");
				reader.next();
			}
		}
		return c;
	}
	
	static void descobrir(int f, int c) {
		int d=0;
		if(mines[f][c]==1) {
			camp[f][c]=10;
		}else {
			if (c + 1 < mines[f].length && mines[f][c + 1]==1) {
				d++;
				
			}
			if (c - 1 >= 0 && mines[f][c - 1]==1) {
				d++;
				
			}
			if (f - 1 >= 0 && mines[f - 1][c]==1) {
				d++;
				
			}
			if (f + 1 < mines.length && mines[f + 1][c]==1) {
				d++;
				
			}
			if (f - 1 >= 0) {
				if (c + 1 < mines[f].length && mines[f - 1][c + 1]==1) {
					d++;
				}
				
			}
			if (f + 1 < mines.length) {
				if (c + 1 < mines[f].length && mines[f + 1][c + 1]==1) {
					d++;
				}
				
			}
			if (f - 1 >= 0) {
				if (c - 1 >= 0 && mines[f - 1][c - 1]==1) {
					d++;
				}
				
			}
			if (f + 1 < mines.length) {
				if (c - 1 >= 0 && mines[f + 1][c - 1]==1) {
					d++;
				}	
			}
			
			camp[f][c]=d;
		}

	}
	static void omplirRanking(String nom, int contadorguanyadors) {
		ranking[contadorguanyadors]=nom;
		contadorguanyadors++;
		if(contadorguanyadors==5) {
			contadorguanyadors=0;
		}
		
	}
	
	static void ranking() {
		System.out.println("Els jugadors que han superat els buscamines s�n: ");
		for(int i=0; i<ranking.length; i++) {
			System.out.print(ranking[i]+", ");
		}
		
	}

}
