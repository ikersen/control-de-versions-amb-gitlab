import java.util.Scanner;

public class JocDeLaVida {
	static Scanner reader = new Scanner(System.in);
	static int files=0;
	static int columnes=0;

	public static void main(String[] args) {
		//s la representaci del joc de la vida
		char tauler[][]= new char [0][0];
		int opcio = 0;
		boolean iniciat = false;

		do {
			// Mostrem el men
			System.out.println("\n\nMen d'opcions: ");
			System.out.println("1. Inicialitzar tauler");
			System.out.println("2. Visualitzar tauler");
			System.out.println("3. Iterar");
			System.out.println("0. Sortir");
			System.out.print("Introdueix una opci: ");

			boolean numcorrecte = false;
			
			
			while (!numcorrecte) {
				if (reader.hasNextInt()) {
					opcio = reader.nextInt();
					numcorrecte = true;
				} else {
					System.out.print("Ha de ser un nmero enter, introdueix un altre: ");
					reader.next();
				}
			}

			switch (opcio) {
			case 0: // Sortir
				System.out.println("Adu!");
				break;
			case 1:
				tauler=inicialitzarTauler();
				iniciat=true;
				break;
			case 2:
				if(iniciat) {
					visualitzarTauler(tauler);
				}else {
					System.out.println("Has de iniciar el tauler prviament amb la opci 1!");
				}
				break;
			case 3:
				if(iniciat) {
					tauler=iterar(tauler);
				}else {
					System.out.println("Has de iniciar el tauler prviament amb la opci 1!");
				}
				break;

			default:
				System.out.println("Opci incorrecta, ha d'estar entre 0 i 3");
				break;
			}

		} while (opcio != 0);

	}

	static char [][] inicialitzarTauler() {
		System.out.print("Introdueix les files del tauler (entre 4 i 10): ");
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				files = reader.nextInt();
				if (files >= 4 && files <= 10) {
					numcorrecte = true;
				} else {
					System.out.print("El nmero de files no s correcte, introdueix un altre: ");
					reader.next();
				}
			} else {
				System.out.print("Ha de ser un nmero enter, introdueix un altre: ");
				reader.next();
			}
		}
		System.out.print("Introdueix les columnes del tauler (entre 4 i 10): ");
		numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				columnes = reader.nextInt();
				if (columnes >= 4 && columnes <= 10) {
					numcorrecte = true;
				} else {
					System.out.print("El nmero de columnes no s correcte, introdueix un altre: ");
					reader.next();
				}
			} else {
				System.out.print("Ha de ser un nmero enter, introdueix un altre: ");
				reader.next();
			}
		}

		char tauler[][] = new char[files][columnes];

		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				tauler[f][c] = 'X';
			}
		}

		int max = files * columnes / 2;
		int min = files * columnes / 4;
		int range = max - min + 1;

		int numvives = (int) (Math.random() * range) + min;

		while (numvives > 0) {
			max = files - 1;
			min = 0;
			range = max - min + 1;
			int fila = (int) (Math.random() * range) + min;
			max = columnes - 1;
			min = 0;
			range = max - min + 1;
			int columna = (int) (Math.random() * range) + min;
			tauler[fila][columna] = '#';
			numvives--;
		}

		
		return tauler;

	}

	static void visualitzarTauler(char[][] tauler) {
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				System.out.print(tauler[f][c] + "\t");
			}
			System.out.println();
		}
		System.out.println("# = Cllules vives\nX = Cllules mortes");

	}
static char[][] iterar(char[][] tauler) {					//La funcio per posar en marcha el videojoc que trucara a altres funcions dins seva
		
		int iteracions = -1;
		
		
		while (iteracions < 0 || iteracions > 100) {
			System.out.print("Escriu un numero enter d'iteracions entre 0 i 100: ");
			iteracions = reader.nextInt();
			
		}
		
		for (int comptador = 0; comptador < iteracions; comptador++) {				
			
			System.out.println("\n-------------------------" + "\nIteracio " + (comptador + 1));
			
			tauler = evoluciona(tauler);
			visualitzarTauler(tauler);
		}
		
		System.out.println("\nEl joc s'ha executat amb exit");
		
		return tauler;
		
	}
	static char[][] evoluciona(char[][] taulerNou) {			
		
		int comptadorCel = 0;
		
		char[][] secret = new char[files][columnes];
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				secret[i][j] = 'X';
			}
		}
		
		for (int i = 0; i < taulerNou.length; i++) {
			for (int j = 0; j < taulerNou[i].length; j++) {
				
				if (taulerNou[i][j]=='X') {
					if (j + 1 < taulerNou[i].length && taulerNou[i][j + 1]=='#') {
						comptadorCel++;
						
					}
					if (j - 1 >= 0 && taulerNou[i][j - 1]=='#') {
						comptadorCel++;
						
					}
					if (i - 1 >= 0 && taulerNou[i - 1][j]=='#') {
						comptadorCel++;
						
					}
					if (i + 1 < taulerNou.length && taulerNou[i + 1][j]=='#') {
						comptadorCel++;
						
					}
					if (i - 1 >= 0) {
						if (j + 1 < taulerNou[i].length && taulerNou[i - 1][j + 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i + 1 < taulerNou.length) {
						if (j + 1 < taulerNou[i].length && taulerNou[i + 1][j + 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i - 1 >= 0) {
						if (j - 1 >= 0 && taulerNou[i - 1][j - 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i + 1 < taulerNou.length) {
						if (j - 1 >= 0 && taulerNou[i + 1][j - 1]=='#') {
							comptadorCel++;
						}
						
					}
					
					if (comptadorCel == 3) {
						secret[i][j] = '#';
						
					}
					
				}else {
					if (j + 1 < taulerNou[i].length && taulerNou[i][j + 1]=='#') {
						comptadorCel++;
						
					}
					if (j - 1 >= 0 && taulerNou[i][j - 1]=='#') {
						comptadorCel++;
						
					}
					if (i - 1 >= 0 && taulerNou[i - 1][j]=='#') {
						comptadorCel++;
						
					}
					if (i + 1 < taulerNou.length && taulerNou[i + 1][j]=='#') {
						comptadorCel++;
						
					}
					if (i - 1 >= 0) {
						if (j + 1 < taulerNou[i].length && taulerNou[i - 1][j + 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i + 1 < taulerNou.length) {
						if (j + 1 < taulerNou[i].length && taulerNou[i + 1][j + 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i - 1 >= 0) {
						if (j - 1 >= 0 && taulerNou[i - 1][j - 1]=='#') {
							comptadorCel++;
						}
						
					}
					if (i + 1 < taulerNou.length) {
						if (j - 1 >= 0 && taulerNou[i + 1][j - 1]=='#') {
							comptadorCel++;
						}
						
					}
					
					if (comptadorCel <= 3 && comptadorCel >= 2) {
						secret[i][j] = '#';
						
					}
					
				}	
				
				comptadorCel = 0;
			}
		}
		
		return secret;
	}

}
