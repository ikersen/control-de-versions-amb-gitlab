import java.text.DecimalFormat;
import java.util.Scanner;

public class MiniMolina {
	static boolean temperaturaintroduida = false;
	static boolean maxmintroduida = false;
	static Scanner reader = new Scanner(System.in);
	static double[] tempdies = new double[24];
	static double[] tempmax= new double[30];
	static double[] tempmin = new double[30];
	static int dia = 0;

	public static void main(String[] args) {
		int opcio = 1;
		while (opcio != 8) {
			mostraMenu();
			opcio = llegirNum();
			realitzaAccio(opcio);
		}

	}

	static void mostraMenu() {
		System.out.println("S�c en MiniMolina, i registro les temperatures d'un mes a Sabadell:");
		System.out.println("1. Introduir temperatura");
		System.out.println("2. Llistat de les temperatures del dia");
		System.out.println("3. Modificar una temperatura del dia");
		System.out.println("4. Enregistrar m�nim i m�xim diari");
		System.out.println("5. Mostrar temperatura m�xima i m�nima d'un dia en concret");
		System.out.println("6. Llistar temperatures m�nimes i m�ximes");
		System.out.println("7. Veure estad�stiques de les temperatures");
		System.out.println("8. Sortir");

	}

	static int llegirNum() {
		int numficat = 0;
		boolean numcorrecte = false;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				numficat = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}

		}
		return numficat;

	}

	static void realitzaAccio(int opcioescollida) {
		switch (opcioescollida) {
		case 1:
			introduirTemperatura();
			dia++;
			break;
		case 2:
			if (temperaturaintroduida) {
				mostraTemperaturesDia();
			} else {
				System.out.println("No hi ha temperatures");
			}
			break;
		case 3:
			if (temperaturaintroduida) {
				modificaTemperatura();
			} else {
				System.out.println("No hi ha temperatures");
			}
			break;
		case 4:
			if (temperaturaintroduida) {
				enregistraMaxMin();
				maxmintroduida=true;
			} else {
				System.out.println("No hi ha temperatures");
			}
			break;
		case 5:
			if (maxmintroduida) {
				mostraMaxMinDia();
			}else {
				System.out.println("Has de enregistrar la temperatura m�xima i m�nima primer amb la opci� 4");
			}
			break;
		case 6:
			if (maxmintroduida) {
				mostraMaxMinMes();
			}else {
				System.out.println("Has de enregistrar la temperatura m�xima i m�nima primer amb la opci� 4");
			}
			break;
		case 8:
			System.out.print("Aqu� acaba el programa");
			break;
		default:
			System.out.println("No �s una opci� v�lida");
		}

	}

	static void introduirTemperatura() {
		double max = 40;
		double min = -10;
		double range = max - min + 1;
		int i = 1;

		tempdies[0] = (double) (Math.random() * range) + min;
		while (i < tempdies.length) {
			tempdies[i] = (double) (Math.random() * range) + min;
			if (Math.abs(tempdies[i - 1] - tempdies[i]) <= 3) {
				i++;
			}
		}
		System.out.println("Temperatures introduides");
		temperaturaintroduida = true;
	}

	static void mostraTemperaturesDia() {
		DecimalFormat formateador = new DecimalFormat("##.#�");
		for (int i = 0; i < 24; i++) {
			System.out.println("A la/es "+i+" la temperatura �s de:"+formateador.format(tempdies[i]) + "\t");
		}
		System.out.println();
	}
	
	static void modificaTemperatura() {
		DecimalFormat formateador = new DecimalFormat("##.#�");
		int i = 0;
		for (i = 0; i < 24; i++) {
			System.out.println("A la/es "+i+" la temperatura �s de:"+formateador.format(tempdies[i]) + "\t");
		}
		System.out.println("\nQuina hora vols modificar?: ");
		boolean numcorrecte = false;
		int horaficada =0;
		while (!numcorrecte) {
			if (reader.hasNextInt()) {
				horaficada = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
		}
		
		while(horaficada>23 || horaficada<0){
			System.out.println("La hora ha de ser desde les 0h a les 23h, introduiex-la un altre cop: ");
			if (reader.hasNextInt()) {
				horaficada = reader.nextInt();
				numcorrecte = true;
			} else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
		}
		
		System.out.println("\nIntroduiex la nova temperatura: ");
		numcorrecte = false;
		double tempficat =0;
		while (!numcorrecte) {
			if (reader.hasNextDouble()) {
				tempficat = reader.nextDouble();
				numcorrecte = true;
			} else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
			}
		}
		
		numcorrecte = false;
		while (!numcorrecte) {
		if(horaficada==0){
			if (Math.abs(tempdies[horaficada + 1] - tempficat) <= 3) {
				tempdies[horaficada]=tempficat;
				numcorrecte = true;
			}else {
				System.out.println("No pot haver una variaci� de m�s de 3� positius o negatius entre dues hores consecutives");
				System.out.println("Torna a introduir una altre temperatura: ");
				tempficat =0;
				if (reader.hasNextDouble()) {
					tempficat = reader.nextDouble();
				} else {
					System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
					reader.next();
				}
				
			}
		}else if(horaficada==23) {
			if (Math.abs(tempdies[horaficada - 1] - tempficat) <= 3) {
				tempdies[horaficada]=tempficat;
				numcorrecte = true;
			}else {
				System.out.println("No pot haver una variaci� de m�s de 3� positius o negatius entre dues hores consecutives");
				System.out.println("Torna a introduir una altre temperatura: ");
				tempficat =0;
				if (reader.hasNextDouble()) {
					tempficat = reader.nextDouble();
				} else {
					System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
					reader.next();
				}
			}
		}else {
			if (Math.abs(tempdies[horaficada - 1] - tempficat) <= 3 && Math.abs(tempdies[horaficada + 1] - tempficat) <= 3 ) {
				tempdies[horaficada]=tempficat;
				numcorrecte = true;
			}else {
				System.out.println("No pot haver una variaci� de m�s de 3� positius o negatius entre dues hores consecutives");
				System.out.println("Torna a introduir una altre temperatura: ");
				tempficat =0;
				if (reader.hasNextDouble()) {
					tempficat = reader.nextDouble();
				} else {
					System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
					reader.next();
				}
			}
		}
		
		}
		
	}
	
	static void enregistraMaxMin() {
		DecimalFormat formateador = new DecimalFormat("##.#�");
		double aux=-10;
		for(int i=0;i<24;i++) {
			if(aux<tempdies[i]) {
				aux=tempdies[i];
			}
		}
		tempmax[dia-1]=aux;
		
		
		aux=40;
		for(int i=0;i<24;i++) {
			if(aux>tempdies[i]) {
				aux=tempdies[i];
			}
		}
		
		tempmin[dia-1]=aux;
		tempdies = new double[24];
	}
	
	static void mostraMaxMinDia() {
		boolean numcorrecte = false;
		int diausuari=0;
		System.out.print("Digues el dia del mes que vulguis saber la seva m�xima i m�nima: ");
		while(!numcorrecte) {
			if (reader.hasNextInt()) {
				diausuari = reader.nextInt();
				numcorrecte=true;
			} else {
				System.out.println("Ha de ser un n�mero enter, introdueix un altre v�lid: ");
				reader.next();
		
			}
		}
		
		if(diausuari>dia || diausuari<1 || diausuari>31 ) {
			System.out.println("El dia que has introduit no es correcte o encara no est� introdu�t");
		}else {
			DecimalFormat formateador = new DecimalFormat("##.#�");
			System.out.println("M�xima "+formateador.format(tempmax[diausuari-1]));
			System.out.println("Minima "+formateador.format(tempmin[diausuari-1]));
		}	
	}
	
	static void mostraMaxMinMes() {
		DecimalFormat formateador = new DecimalFormat("##.#�");
		for (int i = 1; i < tempmax.length; i++) {
			System.out.println("El dia "+i+" la temperatura va ser de m�xima de: "+formateador.format(tempmax[i-1]) + " i m�nima de: "+formateador.format(tempmin[i-1]));
		}
		System.out.println();
	}
	
	
}

