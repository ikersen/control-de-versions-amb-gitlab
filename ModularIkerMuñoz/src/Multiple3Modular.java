public class Multiple3Modular {
	static int sum = 0;

	public static void main(String[] args) {
		enunciat();
		calculaNumsMultiple3();
		mostraResultat();
	}

	static void enunciat() {
		System.out.println("Et dire quants n�meros m�ltiples i quins hi ha entre 3 i 100");
	}

	static void calculaNumsMultiple3() {
		for(int i = 1;i<=100;i++){
			if (i % 3 == 0) {
				sum++;
				System.out.print(i + " ");
			}
		}
	}
	
	static void mostraResultat() {
		System.out.println();
		System.out.print("Hi ha "+sum+" n�meros");
	}
}
